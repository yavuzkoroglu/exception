/**
 * Author: Yavuz Köroğlu
 **/

#include "exception.h"
#include <string.h>
#include <stdio.h>

typedef struct StackTraceElementBody {
    struct StackTraceElementBody * parent;
    char * functionName;
    char * fileName;
    unsigned int lineNo;
} StackTraceElement;

StackTraceElement * trace_top = NULL;

#ifndef _PREVENT_STACK_TRACE_INLINING_
    static inline StackTraceElement * createStackTraceElement
        (const char * functionName, const char * fileName, unsigned int lineNo) 
#else
    static StackTraceElement * createStackTraceElement
        (const char * functionName, const char * fileName, unsigned int lineNo) 
#endif
{
    StackTraceElement * new_elem 
        = (StackTraceElement *)malloc(sizeof(StackTraceElement));
    EASSERT(new_elem != NULL, Exception,
            "\nERROR: Stack Trace Element can't be allocated\n");
    
    new_elem->functionName = (char *)calloc(strlen(functionName) + 1, 1);
    EASSERT(new_elem->functionName != NULL, Exception,
            "\nERROR: Stack Trace Name can't be allocated\n");
    new_elem->functionName = strcpy(new_elem->functionName, functionName);
    
    new_elem->fileName = (char *)calloc(strlen(fileName) + 1, 1);
    EASSERT(new_elem->fileName != NULL, Exception,
            "\nERROR: Stack Trace Name can't be allocated\n");
    new_elem->fileName = strcpy(new_elem->fileName, fileName);
    
    new_elem->lineNo = lineNo;
    
    return new_elem;
}

#ifndef _PREVENT_STACK_TRACE_INLINING_
    static inline void freeTraceElement(StackTraceElement * elem)
#else
    static void freeTraceElement(StackTraceElement * elem)
#endif
{
    free(elem->functionName);
    free(elem->fileName);
    free(elem);
}

#ifndef _PREVENT_STACK_TRACE_INLINING_
    static inline void addStackTraceHelper
        (const char * functionName, const char * fileName, unsigned int lineNo)
#else
    static void addStackTraceHelper
        (const char * functionName, const char * fileName, unsigned int lineNo)
#endif
{
    StackTraceElement * new_elem 
        = createStackTraceElement(functionName, fileName, lineNo);
    new_elem->parent = trace_top;
    trace_top = new_elem;    
}

inline void addStackTrace
    (const char * functionName, const char * fileName, unsigned int lineNo)
{
    #ifdef _ALLOW_STACK_TRACING_
        addStackTraceHelper(functionName, fileName, lineNo);
    #endif
}

#ifndef _PREVENT_STACK_TRACE_INLINING_
    static inline void popStackTraceHelper() 
#else
    static void popStackTraceHelper()
#endif
{
    EASSERT(trace_top != NULL, Exception, "\nERROR: popStackTrace() on empty stack!\n");
    
    if (trace_top->parent == NULL) 
    {
        freeTraceElement(trace_top);
        trace_top = NULL;
    }
    else
    {
        StackTraceElement * temp = trace_top;
        trace_top = trace_top->parent;
        freeTraceElement(temp);
    }
}

inline void popStackTrace() {
    #ifdef _ALLOW_STACK_TRACING_
        popStackTraceHelper();
    #endif
}

#ifndef _PREVENT_STACK_TRACE_INLINING_
    static inline void printStackTraceHelper
        (const char * fileName, unsigned int lineNo)
#else
    static void printStackTraceHelper
        (const char * fileName, unsigned int lineNo)
#endif
{
    fprintf(stderr, " ==> line %u @ %s\n", lineNo, fileName);
    StackTraceElement * itr;
    for(itr = trace_top; itr != NULL; itr = itr->parent) 
    {
        fprintf(stderr, "From %s : line %u @ %s\n", 
                itr->functionName, itr->lineNo, itr->fileName);
    }
}

inline void printStackTrace(const char * fileName, unsigned int lineNo) 
{
    #ifdef _ALLOW_STACK_TRACING_
        printStackTraceHelper(fileName, lineNo);
    #endif
}
