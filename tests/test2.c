#include <stdio.h>
#include "../exception.h"

// Exceptions can be macros
#define onMyException(...) \
    puts(__VA_ARGS__);

// Exceptions can be functions as well!
void onStackTraceException(const char * whatToPrint, int lineNo) {
    puts(whatToPrint);
    printStackTrace(__FILE__, lineNo);
    puts("");
}

void func2() {
    addStackTrace("func2()", __FILE__, __LINE__);
    
    THROW_EXCEPTION(MyException, "NOT FATAL, CONTINUE\n");
    THROW_EXCEPTION(StackTraceException, "PRINT STACK TRACE", __LINE__);
    
    popStackTrace();
}

void func1() {
    addStackTrace("func1()", __FILE__, __LINE__);
    
    func2();

    // REDEFINE EXCEPTION BEHAVIOR
    // Invoke default exception behaviour
    #undef  onMyException
    #define onMyException(...) onException(__VA_ARGS__); 
    
    THROW_EXCEPTION(MyException, "Default Behavior: Terminate the Program\n");
    
    puts("This will never get printed!");
    
    popStackTrace();
}

int main(int argc, char * argv[]) {
    addStackTrace("main()", __FILE__, __LINE__);
    
    func1();
    
    popStackTrace();
}
