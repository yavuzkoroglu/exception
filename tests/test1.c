#include <stdio.h>
#include "../exception.h"

void func2() {
    addStackTrace("func2()", __FILE__, __LINE__);

    THROW_EXCEPTION(Exception, "Exception at");
    
    popStackTrace();
}

void func1() {
    addStackTrace("func1()", __FILE__, __LINE__);
    
    func2();
    
    popStackTrace();
}

int main(int argc, char * argv[]) {
    addStackTrace("main()", __FILE__, __LINE__);
    
    func1();
    
    popStackTrace();
}
