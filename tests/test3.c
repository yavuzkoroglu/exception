#include <stdio.h>
#include "../exception.h"

void func() {
    addStackTrace("func()", __FILE__, __LINE__);

    popStackTrace();
}

int main(int argc, char * argv[]) {
    addStackTrace("main()", __FILE__, __LINE__);
    
    int i;
    for (i = 0; i < 10000000; i++)
    {
        func();
    }
    
    popStackTrace();
}
