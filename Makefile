LIB_NAME = exception
AR = ar -cq
CC = gcc

all:	$(LIB_NAME)_high_performance.o $(LIB_NAME)_low_performance.o $(LIB_NAME)_without_stack.o
	$(info Exception Library is ready.)

$(LIB_NAME)_high_performance.o:	$(LIB_NAME).c $(LIB_NAME).h
				$(info Building for high performance)
				@$(CC) -O3 -D_ALLOW_STACK_TRACING_ $(LIB_NAME).c -c -o $(LIB_NAME)_high_performance.o

$(LIB_NAME)_low_performance.o:	$(LIB_NAME).c $(LIB_NAME).h
				$(info Building for low code size)
				@$(CC) -D_ALLOW_STACK_TRACING_ -D_PREVENT_STACK_TRACE_INLINING_ $(LIB_NAME).c -c -o $(LIB_NAME)_low_performance.o

$(LIB_NAME)_without_stack.o:	$(LIB_NAME).c $(LIB_NAME).h
				$(info Building for non-stack usage)
				@$(CC) -O3 $(LIB_NAME).c -c -o $(LIB_NAME)_without_stack.o
					
clean:	
	$(info Cleaning object files)
	@rm -f $(LIB_NAME)*.o
