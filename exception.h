/**
 * Author: Yavuz Köroğlu
 **/

#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <assert.h>
#include <stdlib.h>
#include <unistd.h>

inline void addStackTrace
    (const char * functionName, const char * fileName, unsigned int lineNo);
inline void popStackTrace();
inline void printStackTrace(const char * fileName, unsigned int lineNo);

#ifndef EASSERT
#define EASSERT(Assertion, Exception, ...)       \
    if (!(Assertion))                            \
    {                                            \
        THROW_EXCEPTION(Exception, __VA_ARGS__); \
    }
#endif

#ifndef THROW_EXCEPTION
#define THROW_EXCEPTION(Exception, ...) \
    on##Exception(__VA_ARGS__)
#endif

#ifndef onException
#define onException(...)                 \
    fprintf(stderr, __VA_ARGS__);        \
    printStackTrace(__FILE__, __LINE__); \
    exit(1)
#endif

#endif
